Bem-vindo ao **Tetris**, nossa plataforma para disseminar o conhecimento da cultura organizacional para desenvolvimento de software!

Portanto, aqui estarão concentradas todas as informações necessárias para a realização das atividades na LabSoft.

Para entender cada um dos itens que você tem acesso, verifique as seções abaixo.

### **Nossa cultura** ###
----

- [Sobre Nós](/nos/sobre-nos.md)

### **Nossos Guias de Processos** ###
----
- [Desenvolvimento](/process/develop/index.md)
- [DEVOPS](/process/devops/index.md)
- [UX](/process/ux/index.md)
- [GTD](/process/gtd/index.md)

### **Materiais Compartilhados com cliente** ###
----
Essa área tem como propósito armazenar diversos materiais e referências técnicas associados que são utilizado pelo cliente ou que o mesmo forneceu em algum momento.

- [CXXXX](/clientes/XXX/index.md)



### **Gestão de Pessoas** ###
----
- [Nossa gestão de pessoas](/career/index.md)
- [Processo Seletivo](/career/index.md)


### **Nossas publicações (internas e externas)** ###
----
- Apresentação (18/09/18) - Cristiano (https://docs.google.com/presentation/d/1cJg5Ybboue6kFYWUnEq2TUzZC-0m-oD2E1Ad41mbIdU/edit#slide=id.p)

