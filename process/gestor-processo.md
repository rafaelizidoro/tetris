﻿O Gestor do Processo no LabSoft (equivalente ao Scrum Master) é a pessoa que tem o maior conhecimento sobre as práticas adotadas no processo de desenvolvimento, além disso deve conhecer e dominar muito bem as práticas e valores dos métodos ágeis usados, de forma a disseminá-lo no grupo.

Devido a esse conhecimento, deve garantir que todos os participantes do processo também conheçam esses princípios, atuando muitas vezes como um professor, e deve assegurar que todos respeitem e sigam o que é definido no processo de desenvolvimento.

Essa função é essencial para que os valores e práticas não se percam quando os squads estão desenvolvendo, o que pode ocorrer principalmente devido a pressões externas sobre as pessoas e as entregas.

Adicionalmente, ao Gestor do Processo conta com um Agile Coach que é responsável também pelo processo a nível organizacional, mas atua no nível de melhoria continua dos processos e para capacitação de todas áreas em práticas ágeis. 

Em alguns momentos o Gestor do Processo e o Agile Coach poderão ser desempenhado por uma única pessoa.