No início de cada novo produto ou funcionadade de grande impacto em um produto existente, temos uma reunião inicial. Na reunião, o time de desenvolvimento de produto (PMs, designers de UX e o squad designado) comunica quais problemas de negócio que estão no escopo desse novo produto ou funcionadade de grande impacto em um produto existente.

Para comunicar esses problemas de negócio, sugere-se utilizar o [Product Vision Board](process/product/templates/The_Product_Vision_Board.pdf)

As notas estão disponíveis em um documento do Google. Consulte o documento para obter detalhes.