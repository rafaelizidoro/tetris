﻿Nosso objetivo é orientar o uso das ferramentas e processos de trabalho do LabSoft, buscando a padronização do preenchimento das tarefas para podermos verificar as atividades em execução e aprimorar os processos, promovendo melhoria contínua.

### **Processo de Desenvolvimento** ###
----

- **Squad de Desenvolvimento (Web e Mobile)**
  * [Product Backlog](/process/develop/products-backlog.md)
  * [Planejamento próxima release/entrega](/process/develop/planejar-entrega-release.md)
  * [Reunião diária](/process/develop/reuniao-diaria.md)
  * Análise de Replanejamento
  * Gestão dos Incidentes (bugs)
  * Retrospectiva das Releases e kickoff novos produtos/projetos
    * [Retrospectivas](/process/develop/Retrospectivas.md)
    * [Kickoff novos produtos/projetos](/process/develop/Kickoff.md)
  * Guias
    * [Padrões de tarefas](/process/develop/guia-padrao-tarefas.md)
    * [Template para Elaborar Incidentes no Jira](/process/develop/guia-modelo-incidente.md)
    * [Boas práticas no uso do Jira](/process/develop/guia-praticas-jira.md)
    * [Template para Elaborar Análise de Negócio no Jira](/process/develop/guia-modelo-analise-negocio.md)
    * [Padrões para especificação em Análise de Negócio](/process/develop/guia-padrao-analise-negocio.md)
    * [Padrões para estilos CSS](/process/develop/guia-padrao-css.md)
    * [Padrões para HTML](/process/develop/guia-padrao-html.md)
    * [Checklist para testes - Portal](guia-testes-checkitens-portal.md)
- **Squad de Produto (Portifólio e Negócio)**
  * [Backlog de Demandas do Produto](/process/develop/products-backlog.md)
  * Análise das Demanda priorizadas
  * [Portfólio Backlog](/process/develop/products-backlog.md)
  * [Proposta para Release](/process/product/proposta-release.md)
  * Aceite da Proposta de Release
  * [Produtos e Roadmap](/process/product/products-roadmap.md)
  * Estratégia dos Produtos
  * [Revisão estratégica e operacional](/process/product/revisao-estrategica-operacional.md)

  
### **Ambientes de desenvolvimento e Arquitetura dos produtos** ###
----

- Mobile/Java


- Web/Java

   - Ambiente padrão


   - [Servidor WildFly de Testes](/process/develop/install-wildfly.md)


- Angular




<script src="https://gitlab.com/trellis/snippets/1753038.js"></script>
