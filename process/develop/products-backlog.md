﻿O **Product Backlog** é formado por uma lista contendo todas as demandas priorizadas e desejadas pelo cliente, para cada um dos produtos desenvolvidos pelo LabSoft. Esta lista de demandas contêm tarefas diretamente relacionadas às demandas solicitadas, tarefas técnicas, defeitos encontrados em homologação e produção, tarefas de pesquisas. Esse são os itens de curto prazo que o cliente aprova um planejamento para desenvolver no produto.

O conteúdo desta lista é definido ou atualizado pelo Comitê do Portifólio (composto pelos Product Owners dos clientes e o LabSoft). O fato de um item estar presente no products backlog não representa o comprometimento de entrega imediata. Os itens do Products Backlog são basicamente formados de: [épicos](/process/develop/epic-story.md) (uma nova funcionalidade ou uma melhoria que reflita em mudanças no processo atual) e [histórias](/process/develop/epic-story.md) (melhorias em partes de funcionalidades existentes, correção de defeitos e correção de processos).

Os itens selecionados para o products backlog pelo comitê do portifólio é proveniente do **Portifólio Backlog**. O Portifólio Backlog é uma lista auxiliar ao product baklog, mas diferente dessa última ela contem diversas demandas que tiveram uma análise de negócio e passaram pela etapa orçamentária com a devida aprovação dos clientes, porém ainda não foram priorizadas pelos clientes.

Além desses dois repositórios de demandas (Products Backlog e Portifólio Backlog), alguns clientes possuem disponível um **Backlog de Oportunidades**. O Backlog de Oportunidades é mantido pelo cliente (product discovery team) e reune um conjunto de demandas (épicos) que ainda são apenas oportunidades de negócio, ideias para melhoria de produtos ou processos, sugestões provenientes de parceiros ou concorrentes. Uma vez que o cliente tenha identificado que a ideia vale a pena ser entregue, é necessário garantir que haja informação suficiente para que o squad a pegue e a faça. Isso exige outro nível de detalhamento, que fazem parte da definição de negócio realizado pelo LabSoft.

Quer saber mais sobre o Backlog de Oportunidades, [clique aqui](https://svpg.com/the-opportunity-backlog/)


