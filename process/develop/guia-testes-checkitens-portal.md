**Entradas de dados**
----
    - Validar dados de entrada
----
    - Mensagens de validação
    O Portal utiliza o padrão de mensagens de validação e mensagens do sistemas através dos arquivos .properties localizados no src/main/resources que são padronizados de acordo com o módulo que ele manipula.
    Ex: **extracao.relatorio.adequacao.taxas.IDSOLICITACAOCADEIA		= ID Retaguarda**
    Esses arquivos são responsáveis por manipular desde dados de cabeçalho da extração de relatórios aos labels e mensagens do sistema.
----
    - Validação campo texto
    - Validação campo númerico


**Relatório**
----
    - Extração/Agendamento de relatórios

    Os relatório seguem um padrão de requisição da url conforme abaixo:

    **.../Cielo/Controller?_rotina=<IDROTINA>&_req=<IDREQ>&_ids=<IDCF_AGENDAMENTO>&DATA=<dd-MM-yyyy>**

    Exemplo:
    **.../Cielo/Controller?_rotina=124&_req=9&_ids=149&DATA=05/10/2018**

    O ID de _rotina e o _req são fixos com os números respectivos 124 e 9 que representam as rotinas de **Executar um Agendamento** na tabela de CF_ROTINA e o valor de Req presente no Atributo PMACESSO da Rotina 124.

    Já o código de **_ids** são os diferentes ids da tabela de tarefa: CF_AGENDAMENTO

    Após rodar o servidor local e logar como administrador é possível aplicar a url no browser para disparar o evento de agendamento de relatório que salvará os arquivos gerados na pasta **/media/hd/cielovendas/report**


**API ou serviço associado**
----
    - dsfsdfsdfsdf


**Clientes**
----
    - Credenciamento
        - sxsadds

