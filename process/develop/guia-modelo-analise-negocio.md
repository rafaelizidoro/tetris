﻿### Template do padrão de especificação de requisitos de negócio no Jira. ###
----

No Jira está disponível para o LabSoft e para o Cliente, uma pendência (número 4706) que trata do MODELO DE ESPECIFICAÇÃO DE NEGÓCIO. Possíveis alterações internas nesse modelo devem ser atualizadas APENAS no template no Jira, visto que o cliente utiliza-se do template para criar as suas pendências.

O conteúdo do modelo deve ser usado no campo "Descrição" das Pendências (ferramenta Jira). O modelo mostrado também possui um exemplo de preenchimento.

Essa Análise Preliminar da Demanda (APD) deve ser identificada e elaborada geralmente em dois momentos:

- na concepção inicial do produto ou durante o desenvolvimento do produto. Entende-se concepção, como uma etapa pré-desenvolvimento, opcional em alguns projetos, onde uma pequena equipe técnica com o apoio de um ou mais Product Owner está compreendendo a demanda solicitada pela organização demandante para validar a viabilidade de negócio, neste momento o APD poderá ser descrita em nível de um épico bem macro e que posteriormente será desmembrado em novos APDs, que serão associados a essa APD, porém cada um detalhará cada necessidade de negócios específica.

- já no segundo caso, uma APD poderá ser identificada e elaborada para atender uma nova demanda ou para a alteração de uma melhoria em necessidade de negócio já desenvolvida durante a construção do produto. Nessa situação não se aplica resoluções de bugs.

O conteúdo do modelo abaixo deve ser usado no campo "Descrição" nas pendências da ferramenta Jira. O modelo mostrado também possui um exemplo de preenchimento.

Para acessar o template, clique no link [CDV-4706](http://jira.xxx.com.br/browse/xxx.md)