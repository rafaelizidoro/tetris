﻿O propjeto Portal utiliza o WildFly como servidor.

### Passos para instalação do servidor integrado ao eclipse para utilização em testes locais: ###

1. Localizar a pasta compartilhada de rede denominada: "xzxzxz V:"

2. Localizar a subpasta Temp/Ferramentas/Wildfly e obtenha o arquivo zipado da versão 12.0.0.

3. Após descompactar o arquivo, coloque-o em uma pasta de sua preferência.

4. Na aba Servers no Eclipse, clique para adicionar um novo servidor.

5. Na nova janela, escolha a opção WildFly 12 e clique em next.

6. Na próxima tela clique em next. Na tela de Add and Remove, se já houver o compilado do projeto em arquivo .ear, adicione e clique em Finish.

Agora o servidor já pode ser iniciado normalmente.