A cada dia do Sprint o squad faz uma reunião, chamada Daily no Scrum. Tem como objetivo disseminar o conhecimento sobre o que foi feito no dia anterior, identificar impedimentos e priorizar o trabalho a ser realizado no dia que se inicia.

As reuniões diárias são realizadas em algum local e horário escolhido pelo squad.

Todos os membros da squad devem participar da reunião diária. Outras pessoas também podem estar presentes, mas como ouvinte. Isso torna as reuniões diárias uma forma para o squad disseminar informações sobre o estado do trabalho.

Uma reunião diária não deve ser usada como momento para resolução de problemas. Questões levantadas devem ser resolvidas depois da reunião e normalmente tratadas por um grupo menor de pessoas que tenham a ver diretamente com o problema ou possam contribuir para solucioná-lo. Durante a reunião, cada membro do squad provê respostas para cada uma destas três perguntas:

- O que está fazendo?
- O que fará depois?
- Há algum impedimento no seu caminho?

O propósito dessas informações é prover para o squad uma excelente compreensão sobre que trabalho foi feito e que trabalho ainda precisa ser feito. A reuinião diária não é uma reunião de status report na qual um chefe fica coletando informações sobre quem está atrasado. Ao invés disso, é uma reunião na qual o squad assume compromissos perante os demais.

Os impedimentos identificados na reunião diária devem ser tratados pelo Scrum Master o mais rapidamente possível.