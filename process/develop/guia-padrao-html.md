## Padrões para utilização do HTML ##

Nomenclaturas:

- Em ID e NAME em geral: Iniciando a primeira palavra em minúscula e as outras maiúsculas.
Preferencialmente os nomes dever estar em inglês.

>Ex:
>```
><div id=”headerMenu”>
><input name=”fullName” type=”text”>
><select id=”comboCountry”>
>Etc…
>```

Comentários:

- Comentários em tags DIV, TABLE e FORM: Uma breve descrição de no máximo 1 linha sobre a função da tag.

>Ex:
>```
><div id=”general”><!—Contém todo o conteúdo estrutural do site -->
>
>	Conteúdo
>
></div><!—fim da div general -->
>```