﻿## Reunião 1:1 (One-on-One) ##

É uma conversa que acontece entre duas pessoas para tratar sobre algum tema que esteja demandando atenção de uma das partes. Elas geram aumento de resultados para o indivíduo e para a organização. Buscam tomar atitudes concretas para reforçar o que há de bom e corrigir o que pode ser melhorado.

Como funciona uma Reunião 1:1

- O liderado traz uma pauta (antecipadamente)
> O foco é no liderado e não no líder, o liderado precisa ter espaço para trazer as demandas que ele sente a necessidade de tratar. O líder deve ter suas pautas previamente preparadas para a reunião, com suas demandas. No final desse texto existem alguns exemplos de assuntos de pauta.

- O líder e liderado realizam uma discussão (20 min)
> A reunião é organizada no formato de 10 mim para o liderado apresentar suas pauta e apontamentos, 10 min para o líder aporesentar a sua pauta e apontamentos e 10 min para o fechamento das ideias. No decorrer da reunião, liderado e líder devem anotar os principais pontos da conversa, para a produção do fechamento final.

- Ambos fazem uma reflexão (fechamento) e traçam metas (10 min)
> Ambos estabelecerem metas a serem cumpridas para a próxima one-on-one. Essas tarefas devem ser estabelecidas tanto para o liderado quanto para o líder. Sugere-se que os 10 min fiquem disponíveis para a reflexão e fechamento das ideias/metas.


O tempo de 30 min não precisa ser seguido a risca, mas é sempre importante realizar a reunião da forma mais dinâmica possível.

Quer saber mais sobre o assunto?
- http://tomtunguz.com/one-on-ones/
- https://getlighthouse.com/blog/how-to-start-one-on-ones-your-teams/
- https://getlighthouse.com/blog/never-cancel-one-on-ones-only-reason-to/


---
Exemplos de Pautas (fonte: Site Resultados Digitais):

- Performance: em que há o acompanhamento de entregas e resultados atingidos até então. Nela, o liderado demonstra a evolução do seu trabalho ou o atingimento de metas. Esse assunto gera muitos inputs para um segundo tema que é desenvolvimento.

- Desenvolvimento: a partir de gaps ou novos desafios, é possível identificar pontos específicos a serem desenvolvidos.

- Feedback: pode ser uma troca de feedbacks sobre comportamentos passados e que podem ser valorizados ou melhorados,tanto da parte do líder quanto do liderado.

- Evolução profissional: ambição, desejos profissionais e experiências que o liderado quer vivenciar diante as oportunidades. Esse tema requer mais tempo de organização, por isso preferencialmente sugere-se trata-lo semestralmente.

- Clima e relacionamento: como o liderado está se sentindo no squad, na relação com pares, no dia a dia do LabSoft.

- Pessoal: Temas pessoais são importantes para criar empatia entre líder e liderado e aumentar a conexão, impactando de forma positiva o indivíduo e organização.