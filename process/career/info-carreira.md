Uma carreira na VEK começa em momentos diferentes para cada pessoa. 

Seja qual for a etapa da sua carreira, você pode esperar oportunidades excelentes aqui na VEK. O mapa de carreira abaixo mostra os caminhos profissionais e o que é preciso para seguir o emprego dos seus sonhos. Com isso, acreditamos no protagonismo e autonomia de cada pessoa para construir uma estrutura justa na empresa.

É assim que funciona:

- Todos os meses, o seu mentor terá uma conversa pessoal com você para alinhar as expectativas, fornecer feedback e ajudá-lo a desenvolver ainda mais.
- A cada três meses, você e seu mentor verificarão os objetivos alcançados e os atualizarão, se necessário.
- A cada 6 meses, você receberá um feedback de todo o time. 

Sua posição pode mudar de acordo com os objetivos que você realizou e o feedback que você recebeu. Se algo não estiver claro, por favor feedback 😉

Abaixo você pode conferir todas as equipes, os responsáveis ​​por cada uma delas e como elas estão conectadas.