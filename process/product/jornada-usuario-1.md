Meta da jornada: o executivo de vendas precisa iniciar o seu dia de trabalho buscando informações sobre o cliente que está visitando.

Acessar o aplicativo
Escolhe a opção Clientes
Visualiza uma lista de Clientes
Escolhe um cliente
Pode realizar ações com as informações do cliente
  - Consulta dados cadastrais
  - Consulta contatos
  - Visualiza o resumo
  - Consulta os equipamentos
  - Consulta o credenciamento
  - Consultar informações de adquirência
  - Consultar ofertas
  - Realizar Contato
  - Consultar Dashboard
  - Consultar ICVA
  - Consultar Riscos

```mermaid
graph LR
    A[Acessar App] -- Menu lateral --> B((Realizar Empenho))
    B -- Escolhe opção --> C((Funcionalidade Clientes))
    C -- Apresenta --> D((Lista de Clientes))
    D -- Escolhe cliente --> E((Realizar as ações permitidas))
    E -- Consulta --> F((Dados cadastrais))
    E -- Consulta --> G((Contatos))
    E -- Visualiza --> I((Resumo))
    E -- Consulta --) J((Equipamentos))
    E -- Consulta --> K((Credenciamento))
   
```

